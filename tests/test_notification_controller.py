import pytest
from botocore.stub import Stubber

from controller.notification_controller import NotificationController


class TestNotificationController:
    notification_stubber = Stubber(NotificationController._sns_client)
    
    def test_existence(self):
        assert NotificationController() is not None and NotificationController is not None
    
    def test_send_notification(self):
        with self.notification_stubber:
            self.notification_stubber.add_response('publish', {})
            
            NotificationController.send_notification("Testing notification controller", ['+17203847569'])
            
            with pytest.raises(Exception):
                NotificationController.send_notification(None, None)
        
