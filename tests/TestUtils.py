import logging
import random

logging.basicConfig(format='[%(asctime)s] %(levelname)s - %(name)s: %(message)s')
log = logging.getLogger('MockObjects')


class MockResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return f'<MockResponse object status_code={self.status_code} json_data={self.json_data}'

    def json(self):
        log.debug(f'Returning mock response {self}')
        return self.json_data


def generate_pronounceable_word(syllable_length=3):
    consonants = 'bcdfghjklmnpqrstvwxyz'
    vowels = 'aeiouy'
    word = ''
    for syllable in range(syllable_length):
        word += random.choice(consonants) + random.choice(vowels)
    return word


def generate_address():
    generated = f'{random.randint(0, 9999):04} {random.choice(("n", "e", "s", "w"))} ' \
                f'{generate_pronounceable_word(random.randint(3, 5))} {random.choice(("way", "st", "ct", "circle", "rd"))}'
    return generated.title()
