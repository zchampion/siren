from datetime import datetime

import pandas
import pytest

from controller.condition_controller import ConditionController


class TestConditionController:
    condition_df = pandas.read_csv('../tests/test_data/weather_ex.csv')
    assert not condition_df.empty
    
    def test_existence(self):
        assert ConditionController() is not None and ConditionController() is not None
    
    def test_find_condition(self):
        assert ConditionController.find_condition(
            condition='Sunny',
            operator='in',
            forecast_col='shortForecast',
            weather=self.condition_df.head(1)
        ) is not None
        
        assert ConditionController.find_condition(
            condition='30',
            operator='above',
            forecast_col='temperature',
            weather=self.condition_df.head(1)
        ) is not None
        
        assert ConditionController.find_condition(
            condition='-20',
            operator='less than',
            forecast_col='temperature',
            weather=self.condition_df.head(1)
        ) is False

        with pytest.raises(Exception):
            ConditionController.find_condition(
                condition='Snow',
                operator='surgeon',
                forecast_col='temperature',
                weather=self.condition_df.head(1)
            )

        with pytest.raises(Exception):
            ConditionController.find_condition(
                condition='Snow',
                operator='surgeon',
                forecast_col='the park',
                weather=self.condition_df.head(1)
            )
    
    def test_find_condition_change(self):
        assert ConditionController.find_condition_change(
            condition='Sunny',
            operator='in',
            forecast_col='shortForecast',
            weather=self.condition_df,
            window_hours_ahead=12
        ) == ('beginning after', datetime.fromisoformat('2022-02-17T06:00:00-07:00'))

        assert ConditionController.find_condition_change(
            condition='50',
            operator='above',
            forecast_col='temperature',
            weather=self.condition_df,
            window_hours_ahead=12
        ) == (None, None)

        assert ConditionController.find_condition_change(
            condition='0',
            operator='under',
            forecast_col='temperature',
            weather=self.condition_df,
            window_hours_ahead=12
        ) == (None, None)

        with pytest.raises(Exception):
            ConditionController.find_condition_change(
                condition='0',
                operator='under',
                forecast_col='temperature',
                weather=None,
                window_hours_ahead=12
            )
    
    def test_parse_windspeed(self):
        assert ConditionController.parse_windspeed('8 mph') == (8, 'mph')
        
        with pytest.raises(ValueError):
            ConditionController.parse_windspeed('bull mph')
        
        with pytest.raises(Exception):
            ConditionController.parse_windspeed(None)
