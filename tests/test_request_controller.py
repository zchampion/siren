import logging
from unittest.mock import MagicMock

import pytest
import requests
from botocore.stub import Stubber

from controller.request_controller import RequestController
from TestUtils import MockResponse, generate_address

from test_data.responses import *

SECRET_STRING = 'Do you really think I would save the api key in plaintext in the tests?'

log = logging.getLogger("tests")


class TestRequestController:
    request_controller_mock = RequestController
    secret_stubber = Stubber(request_controller_mock._secret_client)

    mock_requests = requests
    mock_requests.get = MagicMock(name='get')
    
    def test_new_request_controller(self):
        assert RequestController() is not None and RequestController() is not None

    def test_get_current_weather_df(self):
        self.mock_requests.get.side_effect = [
            MockResponse(google_loc_response_text, 200),
            MockResponse(station_json_val, 200),
            MockResponse(observation_station_list_json, 200),
            MockResponse(current_observations_response, 200),
            MockResponse(station_json_val, 200),
            MockResponse(observation_station_list_json, 200),
            MockResponse(current_observations_response, 200),
            MockResponse(nws_unexpected_error, 500),
            MockResponse(gcp_error, 500),
            MockResponse(gcp_no_results, 500),
        ]
        with self.secret_stubber:
            address = generate_address()
            log.info(address)
            
            self.secret_stubber.add_response('get_secret_value', {'SecretString': SECRET_STRING})
            result = RequestController.get_current_weather_df(address)
            assert not result.empty

            self.secret_stubber.add_response('get_secret_value', {'SecretString': SECRET_STRING})
            result = RequestController.get_current_weather_df(address)
            assert not result.empty

            with pytest.raises(Exception):
                self.secret_stubber.add_response('get_secret_value', {'SecretString': SECRET_STRING})
                RequestController.get_current_weather_df(address)
            
            with pytest.raises(Exception):
                self.secret_stubber.add_response('get_secret_value', {'SecretString': SECRET_STRING})
                RequestController.get_current_weather_df(generate_address())

            with pytest.raises(Exception):
                self.secret_stubber.add_response('get_secret_value', {'SecretString': SECRET_STRING})
                RequestController.get_current_weather_df(generate_address())

    def test_get_forecast_df(self):
        self.mock_requests.get.side_effect = [
            MockResponse(google_loc_response_text, 200),
            MockResponse(station_json_val, 200),
            MockResponse(forecast_json_val, 200),
            MockResponse(station_json_val, 200),
            MockResponse(forecast_json_val, 200),
            MockResponse(google_loc_response_text, 200),
            MockResponse(station_json_val, 200),
            MockResponse(nws_unexpected_error, 200),
        ]
        with self.secret_stubber:
            address = generate_address()
            log.info(address)
            
            self.secret_stubber.add_response('get_secret_value', {'SecretString': 'Do you really think I would save the api key in plaintext in the tests?'})
            result = RequestController.get_forecast_df(address)
            assert not result.empty

            # Test caches
            self.secret_stubber.add_response('get_secret_value', {'SecretString': 'Do you really think I would save the api key in plaintext in the tests?'})
            result = RequestController.get_forecast_df(address)
            assert not result.empty

            with pytest.raises(Exception):
                self.secret_stubber.add_response('get_secret_value', {'SecretString': SECRET_STRING})
                res = RequestController.get_forecast_df(generate_address())
                log.warning(res)

    def test_get_next_station(self):
        RequestController.station_cache = observation_station_list_json['features']
        self.mock_requests.get.side_effect = [
            MockResponse(current_observations_missing_temp_response, 200),
            MockResponse(current_observations_response, 200),
            MockResponse(current_observations_missing_temp_response, 200),
            MockResponse(current_observations_response, 200)
        ]
        assert not RequestController.get_next_station().empty
        assert not RequestController.get_next_station().empty
