from datetime import datetime

import pandas

from main import handler
from controller.request_controller import RequestController
from controller.condition_controller import ConditionController
from controller.notification_controller import NotificationController

from unittest.mock import MagicMock


class TestMainHandler:
    weather_df = pandas.read_csv('../tests/test_data/weather_ex.csv')
    
    mock_request_controller = RequestController
    mock_request_controller.get_current_weather_df = MagicMock(name='get_current_weather_df')
    mock_request_controller.get_current_weather_df.return_value = weather_df.head(1)
    mock_request_controller.get_forecast_df = MagicMock(name='get_forecast_df')
    mock_request_controller.get_forecast_df.return_value = weather_df
    
    mock_condition_controller = ConditionController
    mock_condition_controller.find_condition = MagicMock(name='find_condition')
    mock_condition_controller.find_condition_change = MagicMock(name='find_condition_change')
    mock_condition_controller.find_condition_change.return_value = ('beginning after', datetime.fromisoformat('2022-02-17T06:00:00-07:00'))
    
    mock_notification_controller = NotificationController
    mock_notification_controller.send_notification = MagicMock(name='send_notification')
    
    def test_handler(self):
        assert not handler({}, {})
