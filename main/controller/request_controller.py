"""
Variable notes:
    DEFAULT_ADDR = '14500 W Hampden Ave, Morrison, CO 80465'
    GCP_URL = "https://maps.googleapis.com/maps/api/geocode/json"
    NWS_STATION_URL = 'https://api.weather.gov/points/{lat},{long}'
"""
import logging
import os

import boto3
import pandas
import requests


log = logging.getLogger('siren.request_controller')

pandas.set_option('display.max_columns', None)
pandas.set_option('display.width', None)


class RequestController:
    GOOGLE_API_SECRET_KEY = 'GCP_API'
    GCP_URL = os.environ.get('GCP_URL', 'https://maps.googleapis.com/maps/api/geocode/json')
    NWS_STATION_URL = os.environ.get('NWS_FORE_URL', 'https://api.weather.gov/points/{lat},{long}')
    NWS_OBSERVATION_URL = os.environ.get('NWS_OBSV_URL', 'https://api.weather.gov/stations/{station_id}/observations/latest')
    
    # Singleton pattern
    __instance = None

    def __new__(cls):
        if RequestController.__instance is None:
            RequestController.__instance = object.__new__(cls)
        return RequestController.__instance
    
    _secret_client = boto3.client('secretsmanager')
    gcp_api = None
    
    address_cache = {}
    station_cache = []
    station_gen = None
    forecast_cache = {}
    observation_cache = {}

    @classmethod
    def load_secrets(cls):
        # Get the Google Cloud Platform API Key from AWS Secret Manager
        cls.gcp_api = os.environ.get(cls.GOOGLE_API_SECRET_KEY, cls.gcp_api)
        if not cls.gcp_api:
            response = cls._secret_client.get_secret_value(
                SecretId=cls.GOOGLE_API_SECRET_KEY
            )
            cls.gcp_api = response['SecretString']

    @classmethod
    def pull_forecast(cls, latitude, longitude):
        # Get the weather station from the NWS API
        try:
            point_metadata = cls.get_pt_metadata(latitude, longitude)
            forecast_url = point_metadata['forecastHourly']

            # Get the weather forecast from the weather.gov API
            forecast_json = requests.get(forecast_url).json()
            assert forecast_json, "Hourly forecast response missing!"
            if 'status' in forecast_json and forecast_json['status'] >= 300:
                raise Exception(f'Unable to get the weather due to error {forecast_json["title"]} ({forecast_json["status"]}) - {forecast_json["detail"]}')
            assert 'properties' in forecast_json, f"`properties` field missing from NWS hourly forecast response: {forecast_json}"
            assert 'periods' in forecast_json['properties'], f"`periods` field missing from NWS hourly forecast response: {forecast_json}"
            log.debug(f"Got hourly forecast with {len(forecast_json['properties']['periods'])} records.")
            df = pandas.DataFrame(forecast_json['properties']['periods'])
            return df

        except Exception as e:
            log.error(f"Error getting the hourly forecast from NWS due to {type(e).__name__}: {e}")
            raise e

    @classmethod
    def pull_observations(cls, latitude, longitude):
        try:
            point_metadata = cls.get_pt_metadata(latitude, longitude)
            assert 'observationStations' in point_metadata, "'observationStations' not found in NWS point metadata!"
            observation_station_list = cls.get_obs_stations(point_metadata['observationStations'])
            closest_obs_station = observation_station_list[0]

            current_weather_df = cls.get_observations_df(closest_obs_station)
            log.debug(f"Current weather dataframe:\n{current_weather_df}")
            return current_weather_df

        except Exception as e:
            log.error(f"Error getting the current weather observations from NWS due to {type(e).__name__}: {e}")
            raise e

    @classmethod
    def get_observations_df(cls, closest_obs_station):
        observations = requests.get(
            cls.NWS_OBSERVATION_URL.format(station_id=closest_obs_station['properties']['stationIdentifier'])).json()
        # convert units (especially temperature to F)
        observation_data = observations['properties']
        cls.convert_units(observation_data)
        # flatten the observation dictionary
        flat_observation_data = cls.flatten_nws_resp_dict(observation_data)
        current_weather_df = pandas.DataFrame(flat_observation_data, index=[0], columns=flat_observation_data.keys())
        return current_weather_df

    @classmethod
    def get_pt_metadata(cls, latitude, longitude):
        station_json = requests.get(cls.NWS_STATION_URL.format(lat=latitude, long=longitude)).json()
        assert station_json, "NWS Weather station response missing"
        assert 'properties' in station_json, f"`properties` field missing from NWS station response:\n{station_json}"
        assert 'forecastHourly' in station_json[
            'properties'], f"`forecastHourly` URL field missing from NWS station response:\n{station_json}"
        log.debug("Got forecast url from the station.")
        point_metadata = station_json['properties']
        return point_metadata

    @classmethod
    def get_obs_stations(cls, request_url):
        try:
            stations = requests.get(request_url).json()
            assert 'features' in stations, f"'features' not found in station list:\n{stations}"
            cls.station_cache = stations['features']
            return stations['features']
        except Exception as e:
            log.error(f"Error getting the list of observation stations due to {type(e).__name__}: {e}")
            raise e

    @classmethod
    def closest_stations(cls):
        for station in cls.station_cache:
            log.debug(f"Returning station: {station['properties']['stationIdentifier']}")
            next_observations = cls.get_observations_df(station)
            yield next_observations

    @classmethod
    def get_next_station(cls):
        try:
            if not cls.station_gen:
                cls.station_gen = cls.closest_stations()

            log.debug("Getting observations from next station...")
            return next(cls.station_gen)

        except Exception as e:
            log.error(f"Error getting the next observation station due to {type(e).__name__}: {e}")

    @classmethod
    def filter_cols(cls, df, whitelist=None, blacklist=None):
        """
        Filters the columns in the returned dataframe.
        Either whitelist or blacklist must be passed.
        Both lists are mutually exclusive.
        :param df: dataframe to filter on
        :param whitelist: the list of columns to be included to the exclusion of all else
        :param blacklist: the list of columns to exclude
        :return: dataframe filtered
        """
        if whitelist is not None and blacklist is not None:
            err_msg = 'Whitelist and Blacklist are mutually exclusive, yet both are specified.'
            log.error(f'Error in weather_puller.WeatherPuller.filter_cols: {err_msg}')
            raise Exception(err_msg)

        elif whitelist is None and blacklist is None:
            err_msg = 'Whitelist and Blacklist are mutually exclusive, yet neither are specified.'
            log.error(f'Error in weather_puller.WeatherPuller.filter_cols: {err_msg}')
            raise Exception(err_msg)

        elif whitelist is not None:
            df_whitelist = df[whitelist]
            log.debug(f"Whitelisted dataframe:\n{df_whitelist}")
            return df_whitelist

        elif blacklist is not None:
            # Check all the labels in the blacklist for validity so a warning can be raised if they're not in the df.
            for label in blacklist:
                if label not in df.columns:
                    log.warning(f'In weather_puller.WeatherPuller.filter_cols: Label {label} is not in the dataframe\'s columns.')

            # Get all the columns from the df that aren't in the blacklist.
            inferred_whitelist = [col for col in df.columns if col not in blacklist]
            blacklist_df = df[inferred_whitelist]
            log.debug(f"Blacklisted dataframe:\n{blacklist_df}")

            return blacklist_df

    @classmethod
    def get_location(cls, address):
        log.debug(f"Getting location for {address} ...")

        if address in cls.address_cache:
            log.debug(f"Cache value found for {address}")
            return cls.address_cache[address]

        # defining a params dict for the parameters to be sent to the API
        parameters = {'address': address, 'key': cls.gcp_api}

        # sending get request and saving the response as response object
        # params are sent as part of the URL
        response = requests.get(url=cls.GCP_URL, params=parameters)

        # extracting data in json format
        data = response.json()

        if 'error_message' in data:
            log.error(data['error_message'])
            return None, None, None

        elif len(data['results']) < 1:
            log.error('Get location: No results!')
            return None, None, None

        else:
            # extracting latitude, longitude and formatted address
            # of the first matching location
            latitude = data['results'][0]['geometry']['location']['lat']
            longitude = data['results'][0]['geometry']['location']['lng']
            formatted_address = data['results'][0]['formatted_address']

            log.debug(f"Location for address: {latitude}, {longitude} ({formatted_address})")
            cls.address_cache[address] = latitude, longitude, formatted_address
            return latitude, longitude, formatted_address

    @classmethod
    def get_current_weather_df(cls, address):
        try:
            if address in cls.observation_cache:
                return cls.observation_cache[address]
            
            cls.load_secrets()
            lat, long, address = cls.get_location(address)
            current_weather = cls.pull_observations(lat, long)
            
            cls.observation_cache[address] = current_weather
            return current_weather

        except Exception as e:
            log.error(e)
            raise e

    @classmethod
    def get_forecast_df(cls, address):
        try:
            if address in cls.forecast_cache:
                return cls.forecast_cache[address]
            
            cls.load_secrets()
            loc = address
            lat, long, address = cls.get_location(loc)
            weather_df = cls.pull_forecast(lat, long)

            # Filter out columns without possibly actionable information (blank, redundant, or images)
            weather_df = cls.filter_cols(weather_df, blacklist=['number', 'name', 'temperatureTrend', 'icon', 'detailedForecast'])

            # Split the wind speed so it's useable
            weather_df[['windSpeed', 'windUnits']] = weather_df.windSpeed.str.split(expand=True)
            weather_df['windSpeed'] = weather_df['windSpeed'].astype(int)

            cls.forecast_cache[address] = weather_df
            return weather_df

        except Exception as e:
            log.error(e)
            raise e

    @classmethod
    def convert_units(cls, observations):
        """
        Go through the dict and convert any units with a Celsius unit code to Fahrenheit
        :param observations: dictionary of current observations from NWS
        :return:
        """
        for o in observations:
            if type(observations[o]) is dict:
                if observations[o]['unitCode'] == 'wmoUnit:degC':
                    if observations[o]['value'] is not None:
                        observations[o]['value'] = observations[o]['value'] * 1.8 + 32
                    observations[o]['unitCode'] = 'wmoUnit:degF'
                elif observations[o]['unitCode'] == 'wmoUnit:km_h-1':
                    if observations[o]['value'] is not None:
                        observations[o]['value'] *= 0.6214
                    observations[o]['unitCode'] = 'wmoUnit:m_h-1'

    @classmethod
    def flatten_nws_resp_dict(cls, observation_data):
        flat_dict = {}
        for rec in observation_data:
            if type(observation_data[rec]) is dict and observation_data[rec]['value'] is not None:
                flat_dict[rec] = observation_data[rec]['value']
        return flat_dict
