import logging
from datetime import datetime

from controller.request_controller import RequestController

log = logging.getLogger('siren.condition_controller')


class ConditionController:
    # Singleton pattern
    __instance = None

    def __new__(cls):
        if ConditionController.__instance is None:
            ConditionController.__instance = object.__new__(cls)
        return ConditionController.__instance

    @classmethod
    def condition_in_weather_df(cls, condition, operator, forecast_column, weather_window):
        """
        
        :param condition: 
        :param operator: 
        :param forecast_column: 
        :param weather_window: 
        :return: 
        """
        log.debug(f'Weather window: {weather_window}')
        if forecast_column not in weather_window:
            msg = f"Forecast field '{forecast_column}' not available in window."
            log.error(f"{msg} Columns available: {', '.join(list(weather_window.columns))}")
            raise Exception(msg)
        if operator == 'in':
            condition_df_filter = weather_window[forecast_column].str.contains(condition)
        elif operator in ('<', 'less than', 'under', 'below'):
            condition = int(condition)
            condition_df_filter = weather_window[forecast_column] < condition
        elif operator in ('>', 'greater than', 'above', 'over'):
            condition = int(condition)
            condition_df_filter = weather_window[forecast_column] > condition
        else:
            raise Exception(f"Unrecognized operator ({operator}) for condition search!")
    
        records = weather_window[condition_df_filter]
        anti_records = weather_window[~condition_df_filter]
    
        log.debug(f'"{condition}" condition list length: {len(records)}')
        if len(records) > 0:
            log.debug(f'{"First 10 items of " if len(records) > 10 else ""}"{condition}" condition list:\n{records[:10]}')
        log.debug(f'"{condition}" absent condition list length: {len(anti_records)}')
        if len(anti_records) > 0:
            log.debug(f'{"First 10 items of" if len(anti_records) > 10 else ""}"{condition}" condition list:\n{anti_records[:10]}')
    
        return records, anti_records

    @classmethod
    def find_condition(cls, condition, operator, forecast_col, weather):
        """
        
        :param condition: 
        :param operator: 
        :param forecast_col: 
        :param weather: 
        :return: 
        """
        try:
            assert weather is not None, f"No weather forecast passed to find the condition '{condition} {operator} {forecast_col}'!"

            loops, max_loops = 0, 5
            while loops < max_loops:
                try:
                    loops += 1
                    records, antirecords = cls.condition_in_weather_df(condition, operator, forecast_col, weather)
                    break

                except Exception as e:
                    log.warning(f"Unable to find condition due to {type(e).__name__}: {e}")
                    if loops >= max_loops:
                        raise e
                    weather = RequestController.get_next_station()

            if len(records) > 0:
                message = f'{condition} happening now!'
                log.info(f"Message: {message}")
                return True

            else:
                log.debug(f"Condition absent.")
                return False

        except Exception as e:
            log.error(f'Error while checking for the condition due to {type(e).__name__}: {e}')
            raise e

    @classmethod
    def find_condition_change(cls, condition, operator, forecast_col, weather, window_hours_ahead=2):
        try:
            assert weather is not None, f"No weather forecast passed to find the condition '{condition} {operator} {forecast_col}'!"

            weather_window = weather.head(window_hours_ahead)
            log.debug(f'Looking for snow in shortForecast in window:\n{weather_window}')

            records, anti_records = cls.condition_in_weather_df(condition, operator, forecast_col, weather_window)
            if len(records) > 0 and len(anti_records) > 0:
                records_start = datetime.fromisoformat(records.min()['startTime'])
                anti_records_start = datetime.fromisoformat(anti_records.min()['startTime'])
                if records_start > anti_records_start:
                    message = 'beginning after'
                    start_time = records_start
                else:
                    message = f'ending by'
                    start_time = anti_records_start

                return message, start_time

            else:
                if len(records) > 0:
                    log.debug(f"Condition continuing.")
                else:
                    log.debug(f"Condition absent.")
                return None, None

        except Exception as e:
            log.error(f'Error while checking for the condition change due to {type(e).__name__}: {e}')
            raise e

    @classmethod
    def parse_windspeed(cls, windspeed_string):
        """
        Parses the windspeed as returned in the table from NWS.
        Raw looks like `7 mph` or `20 mph`, direction is in a different column.
        :param windspeed_string: the raw windspeed as a string
        :return: a tuple with an int of the vector magnitude and a string of the units
        """
        try:
            string_split = windspeed_string.split()
            speed = int(string_split[0])
            units = string_split[1]
            return speed, units

        except ValueError as e:
            log.error(f'Error parsing the windspeed in condition_flagger.ConditionFlagger.parse_windspeed - {e}!')
            raise e

        except Exception as e:
            log.error(f'Error in condition_flagger.ConditionFlagger.parse_windspeed - {e}')
            raise e
