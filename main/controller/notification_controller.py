import logging
from pprint import PrettyPrinter

import boto3
from botocore.exceptions import ClientError

log = logging.getLogger('siren.notification_controller')
pp = PrettyPrinter(indent=2)


class NotificationController:
    # Singleton pattern
    __instance = None

    def __new__(cls):
        if NotificationController.__instance is None:
            NotificationController.__instance = object.__new__(cls)
        return NotificationController.__instance
    
    _sns_client = boto3.client('sns')

    @classmethod
    def send_notification(cls, message, destination_list):
        try:
            log.debug(f"Sending '{message}' to {destination_list} ...")
            for destination in destination_list:
                response = cls._sns_client.publish(
                    PhoneNumber=destination,
                    Message=message
                )
                log.debug(f"Message sent to {destination} response:\n{pp.pformat(response)}")

        except (ClientError, Exception) as e:
            msg = f"{type(e).__name__}: {e}"
            log.error(msg)
            raise e

