import logging
import os.path
from datetime import datetime

import yaml
from pytz import timezone

from controller.condition_controller import ConditionController
from controller.notification_controller import NotificationController
from controller.request_controller import RequestController

logging.basicConfig(format='%(levelname)s - %(name)s: %(message)s')
log = logging.getLogger('siren')
log.setLevel(logging.DEBUG)

SUBSCRIPTIONS_FILE = 'subscriptions.yaml'
DATE_FORMAT = '%A, %b %-d at %-I:%M %p'
ADDRESS = '14500 W Hampden Ave, Morrison, CO 80465'


def handler(event, context):
    log.debug("Entered main.handler")
    try:
        with open(SUBSCRIPTIONS_FILE) as subs:
            subscriptions = yaml.safe_load(subs)

        for number in subscriptions:
            for sub in subscriptions[number]:
                log.info(f"Evaluating sub {sub} for {number}...")
                if type(sub['HoursAhead']) is int and sub['HoursAhead'] == 0:
                    # Flag current weather conditions using only the one record in the current weather df
                    current_weather = RequestController.get_current_weather_df(ADDRESS)
                    condition_present = ConditionController.find_condition(
                        condition=sub['Condition'],
                        operator=sub['Operator'],
                        forecast_col=sub['ForecastColumn'],
                        weather=current_weather
                    )

                    if condition_present:
                        NotificationController.send_notification(sub['Message'], [number])
                    else:
                        log.info("No message sent.")

                else:
                    forecast_weather = RequestController.get_forecast_df(ADDRESS)
                    if type(sub['HoursAhead']) is list:
                        window = max(sub['HoursAhead'])
                    else:
                        window = sub['HoursAhead']

                    starting, time = ConditionController.find_condition_change(
                        condition=sub['Condition'],
                        operator=sub['Operator'],
                        forecast_col=sub['ForecastColumn'],
                        weather=forecast_weather,
                        window_hours_ahead=window
                    )

                    if time is not None:
                        hours_to_event = (time - datetime.now(timezone('America/Denver'))).seconds // 3600 + 1
                        if (type(sub['HoursAhead']) is list and hours_to_event in sub['HoursAhead']) or \
                           (type(sub['HoursAhead']) is int and hours_to_event == sub['HoursAhead']):
                            message = sub['Message'].format(starting, time.strftime(DATE_FORMAT))
                            NotificationController.send_notification(message, [number])
                            log.info(f"Message sent: {message}")
                        else:
                            log.info(f"Hours to event is {hours_to_event}. No message sent.")
                    else:
                        log.info("No message sent.")

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        log.error(msg)
        raise e


def bullshit(*args, **kwargs):
    return "bullshit"


if __name__ == "__main__":
    log.info(handler({}, None))
