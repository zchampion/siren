class Subscription:
    def __init__(self, Name, AlertType, Condition, Operator, ForecastColumn, Message=None):
        self.name = Name
        self.alert_type = AlertType
        self.condition = Condition
        self.operator = Operator
        self.forecast_column = ForecastColumn
        self.message = Message
