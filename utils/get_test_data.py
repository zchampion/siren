import logging
import pandas
import os

from rich.logging import RichHandler

from controller.request_controller import RequestController

FORMAT = "%(message)s"
logging.basicConfig(
    level="DEBUG", format=FORMAT, datefmt="[%X]", handlers=[RichHandler()]
)

log = logging.getLogger("rich")


def manual_get_test_data():
    log.info(f'Starting to get manual data...')
    pandas.set_option('display.max_columns', None)
    pandas.set_option('display.width', None)
    # Get test data
    test_dir = 'test_data'
    test_name = 'weather_ex'
    cur_dir = os.path.realpath(os.curdir)
    output_file = os.path.join(cur_dir, test_dir, f'{test_name}.csv')
    output_file_idx = 0
    while os.path.exists(output_file):
        output_file_idx += 1
        output_file = os.path.join(cur_dir, test_dir, f'{test_name}_{output_file_idx}.csv')
    weather_df = RequestController().get_forecast_df()
    print(weather_df.head(24))
    log.info(f'Manually got data, outputting to {output_file}')
    weather_df.to_csv(output_file)


if __name__ == '__main__':
    manual_get_test_data()
