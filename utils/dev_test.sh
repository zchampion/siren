#!/usr/bin/env bash

BRANCH="$(git rev-parse --abbrev-ref HEAD)-"
RUNDIR=$CWD
cd main || exit;

rich -u "Running unit tests";
for t in ../tests/test_*.py
do
  # shellcheck disable=SC1087
  rich -p "[b]Running tests in [blue]$t[/blue]...[/b]"
  coverage run -m pytest -q "$t"
  code=$?
  if [ $code -ne 0 ];
  then
    exit;
  fi
done
coverage report || exit;
coverage html;
cd .. || exit;

rich -u "Starting Build for branch $BRANCH";
if [[ "$BRANCH" == "master" ]];
then
  BRANCH=''
fi
sed "s/{env}/$BRANCH/" meta-template.yaml > template.yaml
sed "s/{env}/$BRANCH/" meta-samconfig.toml > samconfig.toml

sam build --use-container || exit;

rich -u "Testing with Local Invocation";
sam local invoke | rich - --json;

if [[ "$1" == "deploy" ]];
then
  rich -u "[b]Deploying[/b]";
  sam deploy
fi

rm -rf template.yaml samconfig.toml
