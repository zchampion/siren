import logging
import os
from datetime import datetime

LOG_DIR = 'siren-logs'
LOG_FILE = datetime.now().date().isoformat() + '.log'

def set_logger(name):
    """
    Create and set up the logger for the project
    :param name: The name of the logger
    :return: the logger object
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    # create file handler which logs even debug messages
    fh = logging.FileHandler(os.path.join(LOG_DIR, LOG_FILE))
    fh.setLevel(logging.DEBUG)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)

    # create formatter and add it to the handlers
    formatter = logging.Formatter('[%(asctime)s] %(levelname)s - %(name)s: %(message)s')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)

    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)

    return logger